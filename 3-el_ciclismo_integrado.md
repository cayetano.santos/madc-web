## ​ El Ciclismo Integrado

### ¿Qué es el ciclismo integrado y cuáles son sus ventajas?

Es la idea básica que impulsamos en nuestra asociación: el ciclismo urbano como un medio de
transporte en igualdad frente a los demás medios.

La idea subyacente es que la bicicleta es un vehículo, el lugar natural de la bici es la calzada, y
la consecuencia directa es que debe prestarse toda la atención posible a las calles para mejorar su
“ciclabilidad” y permitir el tráfico integrado de las bicicletas. Otra consecuencia directa es la
necesidad de desarrollar la educación de los ciclistas para que conduzcan sus bicicletas como
vehículos.

Según el planteamiento integrador, las vías segregadas ciclistas y en general las infraestructuras
específicamente “ciclistas” son un último recurso que debe ser evitado hasta que todas las otras
medidas integradoras posibles hayan mostrado ser inaplicables.

Los integrantes de Madrid Ciclista somos gente que va en bici por la ciudad cada día. Usamos la bici
como transporte, y sabemos que nuestra seguridad depende tanto de nuestro comportamiento como del de
los demás conductores, y de la colaboración entre ambos.

Según las estadísticas disponibles, circular en bici por la ciudad es menos peligroso de lo que
habitualmente se cree. La bici urbana es mucho menos peligrosa, por ejemplo, que el ciclomotor.

### Principios básicos de organización de un tráfico seguro en ciudad

**Las normas de conducción, el diseño y la señalización de las vías han de ser tan simples como sea
posible, y homogéneas e iguales para todos.**

Este principio es importante porque la simplicidad y la homogeneidad de las normas es lo que
garantiza que todos los usuarios de la vía interpretan de forma similar las situaciones y todos se
comportan de forma predecible unos con otros, lo que es esencial para la seguridad de todos. Cada
excepción, anomalía o complicación innecesaria que se crea en la red viaria o en las normas es un
elemento que disminuye la seguridad, y todas las anomalías introducidas por las vías segregadas
ciclistas redundan en una grave inseguridad para los ciclistas que las usan.

Un aspecto importante de esto es que, mientras que una de las justificaciones que se hacen de las
vías segregadas es que sirven para “proteger” a los ciclistas de conductores desaprensivos (que son
una minoría), en realidad aumentan el potencial de errores y conflictos con los conductores
“respetuosos” (que son la mayoría).

**La posición de un vehículo en la calzada no depende del tipo de vehículo, sino de su velocidad y
de las maniobras que necesite hacer.**

* En principio, los vehículos más lentos circulan más a la derecha, y los más rápidos, más a la
  izquierda. De forma normal, los vehículos más rápidos adelantan por la izquierda.

* Un vehículo que se dispone a girar a la derecha debe posicionarse anticipadamente hacia la derecha
  de la calzada. Análogamente, un vehículo que se dispone a girar a la izquierda debe posicionarse
  anticipadamente a la izquierda del carril, (próximo a la mediana, permitiendo que quienes no van a
  girar le adelanten por la derecha mientras realiza las maniobras del giro).

La vías segregadas rompen con estos principios de seguridad, obligando a las bicis a permanecer a la
derecha independientemente de su velocidad o de las maniobras que necesiten hacer. Además, obligan a
los automovilistas (a los automovilistas respetuosos) a mantenerse fuera del carril bici hasta el
último momento, incluso en situaciones (como giros a la derecha) en las que lo más seguro sería
invadir anticipadamente la vía segregada. Esto, por ejemplo, está contemplado en los carriles bus,
que en muchas ciudades desaparecen unos metros antes de un giro a la derecha para continuar después
del cruce. Por supuesto, la envergadura de un autobús le permite salir del carril bus en las
intersecciones con una seguridad que es simplemente inalcanzable para las bicis.

### La opción alternativa: la segregación ciclista

La postura contraria al ciclismo integrado consiste en la segregación del tráfico: la creencia de
que distintos tipos de vehículos deben estar separados al circular incluso cuando lo están haciendo
por la misma vía. El concepto de segregación es diametralmente opuesto a uno de los principios
básicos de organización del tráfico:*“todos los vehículos deben poder circular actuando entre ellos
según unas normas homogéneas e iguales para todos”*.

Las vías segregadas ciclistas, comúnmente llamadas «carriles-bici», presentan los siguientes
problemas:

 1. Crean hábitos inadecuados de circulación. Si se segrega sistemáticamente, los conductores están
	menos habituados a interactuar con los ciclistas, y los ciclistas menos preparados para utilizar
	la calzada.

 2. Complican las intersecciones, generando situaciones de riesgo que no existen en los carriles
	compartidos. Es un factor muy relevante en ciudades compactas como las españolas, donde esto
	ocurre cada pocas decenas de metros.

 3. Proporcionan una falsa sensación de seguridad, ya que el único tipo de accidente que evitan
	(alcance por detrás) es muy poco habitual en ciudad.

 4. Cambian la percepción del riesgo de los ciclistas, que cometen más imprudencias y aumenta el
	incumplimiento de las normas (respeto a las señales, invasión del espacio peatonal, respeto de
	las preferencias de paso… ).

 5. Los carriles bici sacan a los ciclistas de la vista y de la atención de los automovilistas,
	disminuyendo las posibilidades de cooperación, la responsabilidad mutua, e incluso el respeto al
	ciclista cuando decide no usarlos, independientemente de si son o no obligatorios.

 6. Suponen una barrera peatonal al aumentar los flujos de tráfico. Un peatón debe estar atento a
	diferentes flujos de tráfico, con lo que aumentan los tiempos semafóricos, asi como los sentidos
	anómalos si el carril bici es bidireccional, de un vehículo silencioso como la bicicleta.

En resumen, ¿cuál de las siguientes opciones te parece más atractiva?

A – Una bici.

B – Hay que levantar las calles, reorganizar todos los giros, poner carriles a la derecha o a la
izquierda según lo que sea más cómodo para los coches, pintar el suelo de rojo que se ve más, pintar
el suelo de verde que es más ecológico, pintar el suelo de azul, que es calmante, poner a los
usuarios del autobús en islas como si fueran náufragos, pintar carriles en las aceras en la puerta
de los colegios, si todo falla poner separadores, si sigue fallando poner bordillos, jardineras,
muros, llamar periódicamente a un experto para que diga que lo estás haciendo genial.
