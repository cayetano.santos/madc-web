## ​Madrid Ciclista Home

### Sobre Madrid Ciclista

Madrid Ciclista es una asociación que lucha para conservar y ampliar los derechos de los ciclistas
en Madrid. Somos partidarios del ciclismo urbano integrado y defendemos el Modelo Madrid.

Saber más

### El Modelo Madrid

La Ordenanza Municipal del Ayuntamiento de Madrid legitima la circulación de la bicicleta en Madrid.
Esto nos permite circular por el centro del carril, utilizar toda la ciudad, sin esperar a que la
ciudad se adapte a nosotros.

Saber más

### El ciclismo integrado

Es la idea básica que impulsamos en nuestra asociación: el ciclismo urbano como un medio de
transporte en igualdad frente a los demás medios.

Saber más

### Bla, bla

1. Usa el centro del carril. Minimiza riesgos circulando por el centro del carril. Tendrás mayor
   visibilidad y espacio para reaccionar.

2. Respeta las normas. La manera más fácil de conseguir respeto es comprendiendo que compartimos la
   vía con peatones y otros vehículos. Las normas se acuerdan entre todos.

3. Las aceras son de los peatones. En la ciudad todos somos peatones en algún momento. Si necesitas
   usar la acera, desmonta y camina.

4. Por toda la ciudad. La ciudad ya está preparada para los ciclistas. Disfruta hasta el último
   rincón con tu bicicleta.

### ¿Quieres montar por Madrid? Te echamos una mano

Los primeros pasos no son fáciles, por eso desde Madrid Ciclista queremos ayudarte a que tomes la
calzada y la ciudad. Podemos ayudarte para encontrar una ruta de casa al trabajo u otras necesidades
que tengas.

Contacta con nosotros ​

### Cambia el coche por la bicicleta

#### ​Madrid es ciudad para bicis.

Aunque alcaldes y alcaldesas se empeñen en decir que Madrid no es una ciudad para bicis o que
molestamos en la ciudad, el aumento de ciclistas demuestra que moverse en bici es rápido y no tiene
restricciones.

#### Reduce tu huella de carbono.

Madrid se llena de humo, saltan protocolos por contaminación, pero se puede cambiar la dinámica.
Utiliza tu bicicleta, combínala con otros medios de transporte como el cercanías. Limpio, rápido y
seguro.

#### ​El futuro es eléctrico.

Las bicicletas eléctricas pueden hacer más cómodo tu trayecto, ya sean privadas o las que permite
alquilar el ayuntamiento. Con ellas no hay excusa, mantenimiento sencillo y a un precio asequible y
kilómetros casi sin esfuerzo.
