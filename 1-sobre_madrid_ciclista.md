# ​Sobre Madrid Ciclista

## Presentación de Madrid Ciclista

La asociación «Madrid Ciclista» nació en otoño de 2015 para dar voz a aquellos usuarios de la bici
que consideran que las sensibilidades del ciclismo urbano natural, integrado e integrador no estaban
representadas suficientemente ante las instituciones y en la sociedad.

Algunos miembros del colectivo han estado trabajando por la bicicleta individualmente durante años
en varios grupos (Pedalibre, En Bici por Madrid, Getafenbici, la Tabacalera, etc.), mientras que
otros encontraron en la asociación el lugar desde el que comenzar a reivindicar el uso de la bici.

En Madrid Ciclista creemos que la bicicleta es un vehículo y, como tal, debe circular siempre por la
calzada. Asimismo, defendemos que no existe necesidad de infraestructuras que segreguen el tráfico de
bicicletas.

Los carriles bici urbanos crean anomalías en el tráfico que ponen en peligro a ciclistas y a
peatones. Nosotros promovemos el respeto y la cooperación entre todos los usuarios de las vías
públicas, cumpliendo las normas y buscando así hacer de Madrid una ciudad más amable y más segura
para todos.

Conscientes de que el intercambio de opiniones y el fomento de la participación y el acuerdo son
básicos para obtener buenos resultados en una cuestión tan compleja como la movilidad ciclista, nos
integramos en la Coordinadora Ciclista de Madrid desde sus inicios, por ser un foro que reúne a un
gran número de colectivos con diferentes sensibilidades.

«Madrid Ciclista» realizamos nuestros propios aforos de bicicletas y durante los últimos años hemos
organizado múltiples iniciativas en favor del uso integrado de la bici, como:

• Concurso de fotografía y exposición «Foto Madrid Ciclista» en el Centro Cultural Galileo.

• BiciSanFermines: actividad lúdica consistente en invitar a los ciclistas madrileños a recorrer
Madrid ataviados con vestimenta sanferminera.

• BiciMetro de los distritos Tetuán, Centro y Carabanchel: en colaboración con Juntas Municipales de
Distrito y otras asociaciones ciclistas, se invitó a los viandantes a dar un paseo en bici y
mostrarles lo fácil y seguro que es circular por Madrid en bici.

• Lucicleta: actividad lúdica consistente en realizar un recorrido en bici por las iluminaciones
navideñas más llamativas del centro de Madrid.

• Taller no mixto «Mujeres en bici, mujeres sin límite»: taller que persigue la capacitación de las
mujeres para utilizar la bici como vehículo en Madrid y superar miedos y mitos que las limitan.

• Charlas con diferentes Asociaciones de Vecinos.

• #Bicifindes para nuevos usuarios de bicicleta con el fin de acompañarlos, mostrarles conceptos
básicos de circulación segura y normativa y ayudarlos con sus itinerarios más frecuentes.

• También colaboramos activamente en los espacios de participación abiertos por el Ayuntamiento de
Madrid.

• Asimismo, participamos en la Coordinadora Ciclista de Madrid, en el Foro de la Bicicleta y en el
OMUSM (Observatorio de la Movilidad Urbana Sostenible de Madrid).

## ¿Qué proponemos?

En Madrid Ciclista entendemos que ya existe una amplia infraestructura favorable al uso de la
bicicleta, y que esto supone una gran ventaja al poder ser utilizada diariamente tanto por aquellos
que ya nos movemos en bici, como por los que se van incorporando al uso de este medio de transporte.

Aunque también entendemos que no se hace una ciudad ciclista solamente con infraestructuras. También
existen iniciativas y servicios que pueden ayudar a impulsar el uso de la bicicleta en Madrid y que
pasamos a detallar:

### Formación y concienciación

1. Crear un programa sistemático de formación de usuarios de bicicleta en los centros escolares, con al
   menos 10.000 alumnos formados al año, habilitándolos a la circulación en calles normales.

2. Realizar campañas de concienciación con el objetivo de fomentar el respeto de conductores,
   ciclistas y conductores de VMP, como usuarios con igualdad de derechos en las vías de la ciudad.

3. Crear un programa de formación para profesionales del transporte en los sectores del taxi, VTC,
   conductores de autobús de la EMT y de cualquier otra empresa de transporte colectivo que utilice
   el viario urbano de Madrid.

4. Aunar esfuerzos en campañas a favor de la seguridad tales como:

	1. Campañas de concienciación en redes sociales y otros medios.

	2. Campañas de reclamación al Ayuntamiento de Madrid para la reparación de baches y desperfectos
	   en el firme, como la #Bacheclamación.

	3. Acompañamiento de nuevos usuarios de bicicleta para que aprendan normativa básica de seguridad
	   y circulación en sus recorridos habituales (#Bicifindes).

### Infraestructuras

1. Implantación y mejora de infraestructura de aparcamiento y anclaje seguro para bicicletas en
   lugares en los que se prevé una utilización alta de este medio o se quiere promover su uso:

   • Centros educativos, culturales y bibliotecas.

   • Instalaciones deportivas.

   • Oficinas de la administración pública.

   • Lugares de gran afluencia de público: estadios, parques, emplazamientos de conciertos,
   cines, teatros, etc.

   • Edificios oficiales.

   • Polígonos empresariales en los que se detecte uso abusivo de vehículos motorizados: Julián
   Camarillo, Polígono Las Mercedes, barrio Aeropuerto, Polígono Industrial Valverde, Polígono
   Empresarial Villa de Vallecas, etc.

   Situar además las horquillas o lañas para aparcar bicicletas y VMP en la hilera de aparcamiento
   con el fin de no estorbar al tránsito peatonal. Además, debería pensarse la posibilidad de
   instalarlas en oblicuo, de forma que la bicicleta o VMP no sobresalga más que los coches
   aparcados en la hilera o los elementos delimitadores de dicho espacio que se instalen si
   procediera.

2. Implantación de señalización clara para autorizar giros a derecha con luz roja en el semáforo.

3. Mejora en la señalización de las calles residenciales en las que se autoriza a circular en bici en
   sentido contrario.

4. Implantación de medidas de calmado de tráfico en vías en las que se detecta alta velocidad de
   circulación motorizada, como Institución Libre de Enseñanza, Arturo Soria, Paseo de la
   Castellana, etc.: ciclocarriles 30, pasos de peatones alomados, etc.

5. Pintado de ciclocarriles 30 en calles cuyo límite de velocidad aún sea superior.

6. Revisión de los estándares de las trampillas, tapas y rejillas instaladas sobre el pavimento y
   su coeficiente de deslizamiento. Existen multitud de tipos diferentes de tapas y rejillas
   metálicas en la calzada y no parece existir un criterio claro en lo que se refiere a su
   composición, situación y otras características físicas que inciden en la seguridad de los
   ciclistas.

7. Sustitución de resaltos para la reducción de velocidad de vehículos a motor por otras soluciones
   que permitan a los ciclistas pasar sin sufrir bacheo o vibraciones.

8. Ampliación de servicio de bicicletas compartidas BiciMad para conectar los barrios más exteriores
   y aislados y planificar su implantación en zonas de alto uso potencial como los campus
   universitarios.

## ¿Por qué o para qué usamos la bici?

• Porque es divertido.
• Porque es práctico.
• Porque es saludable.
• Porque es moderno.
• Porque es sexy
• Porque es barato.
• Porque te ahorra tiempo.
• Porque te da libertad.
• Porque te hace hablar con los vecinos.
• Porque no consume gasolina.
• Porque te hace más pequeña la ciudad.
• Porque en invierno la calefacción sale gratis.
• Porque en verano tengo ventilación automática.
• Porque tengo una amiga o amigo que va en bici y está encantado !
