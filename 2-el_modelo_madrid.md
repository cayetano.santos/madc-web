## ​El Modelo Madrid

### La bici hoy en Madrid

Que en Madrid está ocurriendo algo distinto al resto de ciudades en torno a la bicicleta es un
hecho. En los últimos años hemos asistido a una progresiva incorporación de las bicis al tráfico, de
forma espontánea y natural como un vehículo más, sin necesidad de grandes infraestructuras y aun
teniendo el inconveniente de la priorización tradicional al coche.

### ¿Cómo hemos llegado hasta aquí?

En este cambio ha sido determinante la incorporación al uso de la bicicleta como medio de transporte
de un gran grupo heterogéneo de ciclistas. Es este salto del ciclismo deportivo y recreativo al
ciclismo de “movilidad” el que ha visibilizado la posibilidad de que mujeres y hombres de cualquier
edad sin ser especialmente ágiles y veloces circulen de manera natural por la ciudad. Esto produce
un efecto imitación “Si esa mujer menuda de 40 años con falda/hombre de 50 años en traje puede bajar
la Gran Vía, igual yo también”.

Por otro lado, la implantación de BiciMad nos ha puesto las bicis al alcance de la mano, creando en
principio una curiosidad “por probar” que ha crecido hasta un número de usuarios constante,
reforzando la idea de la bicicleta como medio de transporte. Al mismo tiempo, los ciclocarriles han
hecho visible de forma gráfica la presencia de las bicicletas en la calzada.

Todo esto ha ocurrido en una ciudad que no ha desarrollado antes, ni a la par, una red de carriles
bici a imitación de los modelos tradicionales (Copenhague, Sevilla…), por lo que las bicicletas, como
vehículos que son, han ocupado su sitio: las calzadas. Y la respuesta del hegemónico coche ha sido
sorprendente: en menos de 2 años los ciclistas percibimos un aumento espectacular del respeto hacia
la bici, asumida en tan poco tiempo como un elemento integrado más del tráfico.

Con frecuencia los ciudadanos nos quejamos de que se diseñan políticas sin consultar a los
colectivos a los que se van a aplicar. Pues bien, en este caso somos los usuarios los que hemos
diseñado un modelo real, contribuyendo cada uno con sus necesidades y características propias, y que
responde a necesidades reales porque lo hemos hecho los ciclistas que a diario nos echamos a la
calzada como un acto cotidiano más. Hemos enterrado aquella frase de Álvarez del Manzano “La
bicicleta no sirve en Madrid”, demostrando que la bici sí sirve, porque Madrid tiene todo lo que hay
que tener para servir: bicis y calzadas. Y personas que piensan “¿por qué no probar?”.

Fue la Ordenanza de Movilidad para la Ciudad de Madrid la que configuró la base de un modelo Madrid,
en concreto su artículo 39 bis estableciendo que «En la calzada, las bicicletas circularán ocupando
la parte central del carril». Esto posiciona la ordenanza como una de las más vanguardistas del
mundo, en oposición al resto de modelos que fijan la posición de la bici en la parte derecha del
carril. De esta manera, no solamente se entiende que el centro del carril es más seguro en términos
de visibilidad para el resto de vehículos y minimiza la posibilidad de accidentes en giros a la
derecha, sino que también apoya una concepción de la bicicleta como vehículo en igualdad de
condiciones con los demás, con pleno derecho a la ocupación de la calzada.

### ¿Qué es lo que define al «Modelo Madrid»?

Es la principal diferencia con el resto de “modelos” ciclistas, basados en la implantación de
carriles-bici segregados. En ellos subyace la idea de la bicicleta como vehículo residual, de
segunda categoría frente los demás, al que hay que apartar del tráfico regular para que “no
entorpezca”. Por origen y desarrollo lo que hacen es encajar las bicicletas en el modelo clásico del
siglo XX diseñado en torno al coche.

El Modelo Madrid no depende de las actuaciones políticas ni de las infraestructuras: su elemento
central es la conducta tanto de ciclistas como de automovilistas: una conducta de normalidad y
respeto mutuo que ha ido construyéndose espontáneamente, pero que tiene implicaciones conceptuales,
políticas y de infraestructura:

* Concepción de la bicicleta como vehículo en igualdad de condiciones respecto a los demás. Es la
  característica que diferencia al modelo que se está construyendo en Madrid frente al resto,
  configurados en torno a la implantación de carriles-bici segregacionistas y que se inspiran en la
  idea de la bicicleta como vehículo residual, de segunda categoría frente los demás, al que hay que
  apartar del tráfico regular para que «no entorpezca», y siendo no tanto modelos en sí mismos, como
  encaje de un elemento más en el modelo clásico del siglo XX diseñado en torno al coche.

* De ello se deriva que el ciclista madrileño consigue empoderarse como tal, como actor principal
  del tráfico en la ciudad, con pleno derecho a circular por la calzada, siguiendo las mismas normas
  que el resto de vehículos y disponiendo de toda la ciudad para moverse. Toda la ciudad es ciclable
  sin necesidad de itinerarios especiales.

* Naturalidad. Nos incorporamos al tráfico sin necesidad de grandes y costosas infraestructuras
  específicas.

* Efecto imitación. El aumento exponencial de ciclistas en calzada que se viene experimentando
  responde, en gran medida a este efecto: cuantos más ciclistas sean visibles en todo tipo de
  calzadas, más potente es la idea «yo también puedo».

* Pérdida del miedo. Al contrario de lo que sucede en los modelos clásicos basados en carriles bici,
  en los que se envía el mensaje de que compartir espacio-calzada con el resto de vehículos es
  peligroso, en el modelo Madrid se visibiliza, con el ejemplo de los ciclista en calzada que es
  seguro, siempre que se actúe con naturalidad y respetando las normas comunes que rigen para todos.

* Poca presencia de bicicletas en aceras, evitando los conflictos con los peatones que se agravan
  enormemente en lugares donde abundan las vías segregadas: los ciclistas que consideran que su
  lugar no es la calzada continúan por la acera.

* Respeto creciente del resto de vehículos. Que se habitúan, y lo han hecho de forma espectacular y
  en muy poco tiempo, a la presencia de bicicletas en la calzada, disminuyendo su velocidad,
  aprendiendo a realizar adelantamientos seguros y respetando distancias de seguridad.

### Participación política en el modelo

Frente a las críticas que se plantean a este «modelo Madrid» por considerarlo anárquico, carente de
ordenación y exigen una mayor implicación de la administración en la configuración de la movilidad,
se puede decir que en ningún caso el apoyo institucional a este modelo implica inacción, es más,
exige un empuje decidido a las bases que el Ayuntamiento de Madrid ha establecido en su favor.

La propia Ordenanza madrileña no ha conseguido escapar completamente a la tradición segregacionista
y mantiene rasgos de la misma que es deseable modificar, el mencionado artículo 39 bis prescribe que
«en las vías con más de un carril circularán siempre por el carril de la derecha», sin contemplar
circunstancia alguna en la que se puedan utilizar los demás carriles salvo el izquierdo «si han de
realizar un giro a la izquierda». Esta rigidez de la norma contraviene la consideración de la
bicicleta como un vehículo más, así como ignora la complejidad del tráfico en una ciudad como Madrid
que exige una adaptación continua y fluida de los vehículos que en ella circulan.

Pero además de lo ya realizado (Ordenanza de Movilidad, BiciMad, ciclocarriles), hay que insistir en
que la inversión en infraestructura que la administración pública puede realizar no pasa
necesariamente por la segregación mediante carriles-bici, existen múltiples acciones en favor del
uso de la bicicleta que el Ayuntamiento puede apoyar: avanzabicis en los semáforos, aparcabicis
fuera de las aceras, extensión de BiciMad a los barrios que se sitúan fuera del anillo M-30, impulso
del programa europeo STARS, campañas de acompañamiento para principiantes en centros de trabajo
siguiendo el modelo «Bicifindes», cursos de reciclaje en normativa a Policía Municipal, Agentes de
Movilidad y conductores EMT, exigencia de las normas de circulación, especialmente los límites de
velocidad, por parte de todos los vehículos …

### ¿A dónde puede llegar el Modelo Madrid?

La ventaja que tiene la tardía incorporación de la bicicleta en Madrid, es que nos permite aprender
de los errores de los que empezaron antes y adaptar sus aciertos a la sostenibilidad de una gran
ciudad del siglo XXI. Los modelos, básicamente centro y norte europeos, tuvieron sentido en la época
de expansión del automóvil, cuando éste implicaba progreso.

Ahora la sociedad se ha dado cuenta de la insostenibilidad de esa concepción de la movilidad y por
tanto ese esquema ha quedado obsoleto. Ciudades como Ámsterdam son en este sentido esclavas de su
pasado y no pueden renunciar al modelo que las define, por eso serán las últimas en unirse a la
tendencia que está marcando Madrid y que será mayoritaria en Europa en pocos años.

Sería un retroceso echar a perder todo lo conseguido hasta ahora sin permitir que este modelo
discreto, espontáneo, que no exige grandes inversiones ni obras, crezca naturalmente. Somos tráfico
y el resto de vehículos nos reconoce como tal porque nos hemos ganado ese derecho. Todos los
ciclistas madrileños podemos estar orgullosos de un modelo que hemos creado entre todos.

Nadie va a venir de Europa ni de ningún sitio a Madrid a ver la reproducción del modelo tradicional
segregacionista. Pero podrían venir a vernos, a los ciclistas madrileños, circular con normalidad, a
ver el mejor tráfico del mundo con relación a los ciclistas, y a preguntarse como lo hemos hecho. Eso
es ser un modelo.
